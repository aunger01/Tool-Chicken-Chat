/*
 * @Author: hua
 * @Date: 2019-04-26 20:00:00
 * @LastEditors: Howard
 * @LastEditTime: 2021-11-27 22:42:58
 */
module.exports = {
  NODE_ENV: '"production"',
  VUE_APP_CLIENT_SOCKET: '"http://139.155.176.209:85"',
  VUE_APP_CLIENT_API: '"http://39.155.176.209:500"',
  VUE_APP_PUBLIC_KEY: `"-----BEGIN PUBLIC KEY-----MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOaQO5ImLVJwdyDYx4c/QdOKbgB0bV5k/4n9UQej0RhegR8PAfy9bSTagR/2hxSsE5vaE4YjYGtSmFrsWfoUyQHbcJGIfSUUYkcE2OMq4mmENk5KbrUemWdFEIp0k/Y7DlPMAGUdt2YeRakY1gzUI9kyZOcuA0ZP6vzwe8wnFtMwIDAQAB-----END PUBLIC KEY-----"`,
};
